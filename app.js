var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var users = require('./routes/users'); //routes are defined here
var questions = require('./routes/questions');
var app = express(); //Create the Express app
//connect to our database
//Ideally you will obtain DB details from a config file
var dbName = 'myish';
var connectionString = 'mongodb://localhost:27017/' + dbName;

mongoose.connect(connectionString);


app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})


//configure body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/api', users); //This is our route middleware
//app.use('/api', questions);
module.exports = app;

