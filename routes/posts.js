var Posts = require('../models/posts');
var express = require('express');
var router = express.Router();
var crypto = require('crypto');
mongoose = require ('mongoose');
ObjectID = require('mongodb').ObjectID;
var Users = require('../models/users')

//const apnoptions = {
//certData: null,
//key: 'MyishDevPush.pem',
//cert: 'aps_dev_cert.pem',
//  keyData: null,
//  passphrase: 'myish@123',
//  ca: null,
//   gateway: 'gateway.sandbox.push.apple.com',
//  port: 2195,
//  enhanced: true,
//  cacheLength: 100//,
//};


const apnoptions = {
  certData: null,
  key: 'MyishPushProd.pem',
  cert: 'prod-cert.pem',
  keyData: null,
  passphrase: '',
  ca: null,
  gateway: 'gateway.push.apple.com',
  port: 2195,
  enhanced: true,
  cacheLength: 100
};



router.route('/posts')
.get(function(req, res) {
  // Posts.find(function(err, Posts) {
  //   if (err) {
  //     return res.send(err);
  //   }
  //   res.json(Posts);
  // });
  Posts.find({}).sort({timestamp: 'asc'}).exec(function(err, Posts) {
    if (err) {
      return res.send(err);
    }
    res.json(Posts);
  });
})
.post(function(req, res) {
  var posts = new Posts(req.body);
  posts.save(function(err,postObjectId) {
    if (err) {

      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
        subject: 'Myish - Error - Posts',
        text: 'Your error is: ' +err
      });


      return res.send(err);
    }

    res.send( postObjectId.id );
  });
});




router.route('/addcomment').post(function(req, res) {

  var postid = req.body.postid;
  var objectId = new ObjectID(postid);
  var comments = req.body.comments;
  var postedbyid = req.body.postedbyid;
  Posts.update({
    "_id": objectId
  },
  {
    "$push": {
      "comments": {
        "userid":postedbyid,"comments":comments
      }
    }
  }
  , function(err, result) {
    if (err)  {

      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
        subject: 'Myish - Error - Add Comment',
        text: 'Your error is: ' +err
      });

      return res.send(err);}
    res.send({ message: 'Comment Added' });
  });
});


router.route('/addcommentdetails').post(function(req, res) {

  var postid = req.body.postid;
  var objectId = new ObjectID(postid);
  var comments = req.body.comments;
  var postedbyid = req.body.postedbyid;
  //var postedbyobjectId = new ObjectID(postedbyid);
  var userimageURL= req.body.profilepictureURL;
  var username= req.body.username;

  Posts.update({
    "_id": objectId
  },
  {
    "$push": {
      "comments": {
        "userid":postedbyid, "comments":comments, "userimage":userimageURL, "username":username,"timestamp":new Date()
      }
    },
    "$inc":{
      "commentcount":+1
    }
  }
  , function(err, result) {
    if (err) {
      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
        subject: 'Myish - Error - Add Comment Details',
        text: 'Your error is: ' +err
      });

      return res.send(err);}
    res.send({ message: 'Comment Added' });
  });
});



router.route('/removecomment').post(function(req, res) {

  var postid = req.body.postid;
  var commentid= req.body.commentid;
  var objectId = new ObjectID(postid);
  var commentObjectId= new ObjectID(commentid);
  Posts.update({
    "_id": objectId
  },
  {
    "$pull": {
      "comments": {
        "_id":commentObjectId
      }
    },
    "$inc":{
      "commentcount":-1
    }
  }
  , function(err, result) {
    if (err) {
      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
        subject: 'Myish - Error - Remove Comments',
        text: 'Your error is: ' +err
      });

      return res.send(err);}
    res.send({ message: 'Comment Removed' });
  });
});


router.route('/postyay').post(function(req, res) {

  var postid = req.body.postid;
  var objectId = new ObjectID(postid);
  var userid = req.body.userid;
  var userObjectId= new ObjectID(userid);
  Posts.update({
    "_id": objectId
  },
  {
    "$push": {
      "postyays": {
        "userid":userObjectId,"timestamp":new Date()
      }
    },
    "$inc":{
      "postyaycount":+1
    }
  }
  , function(err, result) {
    if (err) {return res.send(err);}
    res.send({ message: 'Post Yay Added' });
  });
});

router.route('/postyayall').post(function(req, res) {

  var postid = req.body.postid;
  var objectId = new ObjectID(postid);
  var userid = req.body.userid;
  var userObjectId= new ObjectID(userid);
  Posts.update({
    "_id": objectId
  },
  {
    "$push": {
      "postyays": {
        "userid":userObjectId,"timestamp":new Date()
      },
      "postseenbyuser": {
        "userid":userObjectId,"timestamp":new Date()
      }
    },
    "$inc":{
      "postyaycount":+1
    }
  }
  , function(err, result) {
    if (err) {
      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
        subject: 'Myish - Error - Post Yay All',
        text: 'Your error is: ' +err
      });

      return res.send(err);}
    });

  Users.update({
    "_id": userObjectId
  },
  {
    "$push": {
      "postsseen": {
        "postid":objectId, "timestamp":new Date()
      }
    }
  }
  , function(err, result) {
    if (err)  {
      return res.send(err);
    }

    res.send({ message: 'Yay All Completed' });
  });
});





router.route('/postnay').post(function(req, res) {

  var postid = req.body.postid;
  var objectId = new ObjectID(postid);
  var userid = req.body.userid;
  var userObjectId= new ObjectID(userid);
  Posts.update({
    "_id": objectId
  },
  {
    "$push": {
      "postnays": {
        "userid":userObjectId,"timestamp":new Date()
      }
    },
    "$inc":{
      "postnaycount":+1
    }

  }
  , function(err, result) {
    if (err) {return res.send(err);}
    res.send({ message: 'Post Nay Added' });
  });
});



router.route('/postnayall').post(function(req, res) {

  var postid = req.body.postid;
  var objectId = new ObjectID(postid);
  var userid = req.body.userid;
  var userObjectId= new ObjectID(userid);
  Posts.update({
    "_id": objectId
  },
  {
    "$push": {
      "postnays": {
        "userid":userObjectId,"timestamp":new Date()
      },
      "postseenbyuser": {
        "userid":userObjectId,"timestamp":new Date()
      }
    },
    "$inc":{
      "postnaycount":+1
    }
  }
  , function(err, result) {
    if (err) {
      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
        subject: 'Myish - Error - Post Nay All',
        text: 'Your error is: ' +err
      });

      return res.send(err);}
    });

  Users.update({
    "_id": userObjectId
  },
  {
    "$push": {
      "postsseen": {
        "postid":objectId, "timestamp":new Date()
      }
    }
  }
  , function(err, result) {
    if (err)  {
      return res.send(err);
    }

    res.send({ message: 'Nay All Completed' });
  });
});


router.route('/postskipall').post(function(req, res) {

  var postid = req.body.postid;
  var objectId = new ObjectID(postid);
  var userid = req.body.userid;
  var userObjectId= new ObjectID(userid);
  Posts.update({
    "_id": objectId
  },
  {
    "$push": {
      "postskip": {
        "userid":userObjectId,"timestamp":new Date()
      },
      "postseenbyuser": {
        "userid":userObjectId,"timestamp":new Date()
      }
    },
    "$inc":{
      "postskipcount":+1
    }
  }
  , function(err, result) {
    if (err) {
      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
        subject: 'Myish - Error - Post Skip All',
        text: 'Your error is: ' +err
      });

      return res.send(err);}
    });

  Users.update({
    "_id": userObjectId
  },
  {
    "$push": {
      "postsseen": {
        "postid":objectId, "timestamp":new Date()
      }
    }
  }
  , function(err, result) {
    if (err)  {
      return res.send(err);
    }

    res.send({ message: 'Skip All Completed' });
  });
});



router.route('/postskip').post(function(req, res) {

  var postid = req.body.postid;
  var objectId = new ObjectID(postid);
  var userid = req.body.userid;
  var userObjectId= new ObjectID(userid);
  Posts.update({
    "_id": objectId
  },
  {
    "$push": {
      "postskip": {
        "userid":userObjectId,"timestamp":new Date()
      }
    },
    "$inc":{
      "postskipcount":+1
    }
  }
  , function(err, result) {
    if (err) {return res.send(err);}
    res.send({ message: 'Post Skips Added' });
  });
});

router.route('/postseenbyuser').post(function(req, res) {

  var postid = req.body.postid;
  var objectId = new ObjectID(postid);
  var userid = req.body.userid;
  var userObjectId= new ObjectID(userid);
  Posts.update({
    "_id": objectId
  },
  {
    "$push": {
      "postseenbyuser": {
        "userid":userObjectId,"timestamp":new Date()
      }
    }
  }
  , function(err, result) {
    if (err) {
      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
        subject: 'Myish - Error - Post Seen By User',
        text: 'Your error is: ' +err
      });

      return res.send(err);}
    res.send({ message: 'Post Seen by User Added' });
  });
});


router.route('/reportabusepost').post(function(req, res) {

  var postid = req.body.postid;
  var objectId = new ObjectID(postid);
  var userid = req.body.userid;
  var userObjectId= new ObjectID(userid);
  Posts.update({
    "_id": objectId
  },
  {
    "$push": {
      "reportabuse": {
        "userid":userObjectId,"timestamp":new Date()
      }
    }
  }
  , function(err, result) {
    if (err) {
      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
        subject: 'Myish - Error - Report Abuse Post',
        text: 'Your error is: ' +err
      });

      return res.send(err);}
    res.send({ message: 'Post Reported' });
  });
});

//This post gets the posts for the user along with the limit
router.route('/getpostsforuser').get(function(req, res) {
  var userid = req.query.userid;
  var objectId = new ObjectID(userid);
  var limit = req.query.limit;
  Posts.find({
    "postseenbyuser.userid": {"$ne":objectId}
  }).sort({timestamp: 'desc'}).limit(limit).exec(function(err, Posts) {
      if (err) {
        var nodemailer = require('nodemailer');
        var transporter = nodemailer.createTransport();
        transporter.sendMail({
          from: 'info@myish.com',
          to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
          subject: 'Myish - Error - Get Posts For User',
          text: 'Your error is: ' +err
        });
        return res.send(err);}
      res.json(Posts);
    });
  });

  router.route('/getlast30posts').get(function(req, res) {
    
    var limit = 30;
    Posts.find({}).sort({timestamp: 'desc'}).limit(limit).exec(function(err, Posts) {
        if (err) {
          var nodemailer = require('nodemailer');
          var transporter = nodemailer.createTransport();
          transporter.sendMail({
            from: 'info@myish.com',
            to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
            subject: 'Myish - Error - Get Posts For User',
            text: 'Your error is: ' +err
          });
          return res.send(err);}
        res.json(Posts);
      });
    });


router.route('/searchpost').get(function(req, res) {
  var searchText= req.query.searchtext;
  Posts.find( {"$text": { $search: searchText } },function(err, Posts) {
    if (err) {
      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
        subject: 'Myish - Error - Search Post',
        text: 'Your error is: ' +err
      });
    return res.send(err);
    }
  res.json(Posts);
  });
});

router.route('/newsearchpost/:searchtext/:page').get(function(req, res) {
  var searchText= req.params.searchtext;
  var page = req.params.page;
  var offset = (page != 'undefind') ? page*10 : 0;
  Posts.find( {"$text": { $search: searchText } },function(err, Posts) {
    if (err) {
      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
        subject: 'Myish - Error - Search Post',
        text: 'Your error is: ' +err
      });
    return res.send(err);
    }
  res.json(Posts);
  }).limit(10).skip(offset);
});


  router.route('/getpostsuseryay').get(function(req, res) {
    var userid = req.query.userid;
    var objectId = new ObjectID(userid);

    Posts.find({
      "postyays.userid": objectId}
      , function(err, Posts) {
        if (err) {

          var nodemailer = require('nodemailer');
          var transporter = nodemailer.createTransport();
          transporter.sendMail({
            from: 'info@myish.com',
            to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
            subject: 'Myish - Error - Get Post User Yay',
            text: 'Your error is: ' +err
          });


          return res.send(err);}
        res.json(Posts);
      })
    });


router.route('/newgetpostsuseryay/:userid/:page').get(function(req, res) {
  var userid = req.params.userid;
  var objectId = new ObjectID(userid);
  var page = req.params.page;
  var offset = (page != 'undefind') ? page*24 : 0;
  Posts.find({
    "postyays.userid": objectId}
    , function(err, Posts) {
      if (err) {

        var nodemailer = require('nodemailer');
        var transporter = nodemailer.createTransport();
        transporter.sendMail({
          from: 'info@myish.com',
          to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
          subject: 'Myish - Error - Get Post User Yay',
          text: 'Your error is: ' +err
        });


        return res.send(err);}
      res.json(Posts);
    }).limit(24).skip(offset);
  });



    router.route('/getpostsusernay').get(function(req, res) {
      var userid = req.query.userid;
      var objectId = new ObjectID(userid);
      Posts.find({
        "postnays.userid": objectId}
        , function(err, Posts) {
          if (err) {

            var nodemailer = require('nodemailer');
            var transporter = nodemailer.createTransport();
            transporter.sendMail({
              from: 'info@myish.com',
              to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
              subject: 'Myish - Error - Get Posts User Nay',
              text: 'Your error is: ' +err
            });

            return res.send(err);}
          res.json(Posts);
        })
      });


router.route('/newgetpostsusernay/:userid/:page').get(function(req, res) {
  var userid = req.params.userid;
  var objectId = new ObjectID(userid);
  var page = req.params.page;
  var offset = (page != 'undefind') ? page*24 : 0;
  Posts.find({
    "postnays.userid": objectId}
    , function(err, Posts) {
      if (err) {

        var nodemailer = require('nodemailer');
        var transporter = nodemailer.createTransport();
        transporter.sendMail({
          from: 'info@myish.com',
          to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
          subject: 'Myish - Error - Get Posts User Nay',
          text: 'Your error is: ' +err
        });

        return res.send(err);}
      res.json(Posts);
    }).limit(24).skip(offset);
  });


      router.route('/getpostsuserskip').get(function(req, res) {
        var userid = req.query.userid;
        var objectId = new ObjectID(userid);
        Posts.find({
          "postskip.userid": {"$ne":objectId}}
          , function(err, Posts) {
            if (err) {
              var nodemailer = require('nodemailer');
              var transporter = nodemailer.createTransport();
              transporter.sendMail({
                from: 'info@myish.com',
                to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
                subject: 'Myish - Error - Get Posts User Skip',
                text: 'Your error is: ' +err
              });


              return res.send(err);}
            res.json(Posts);
          })
        });



        router.route('/getcommentsbypostid').get(function(req, res) {
          var postid = req.query.postid;
          var objectId = new ObjectID(postid);
          Posts.find({
            "_id": objectId},{"comments" :1}
            , function(err, Posts) {
              if (err) {

                var nodemailer = require('nodemailer');
                var transporter = nodemailer.createTransport();
                transporter.sendMail({
                  from: 'info@myish.com',
                  to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
                  subject: 'Myish - Error - Get Comments By Post Id',
                  text: 'Your error is: ' +err
                });

                return res.send(err);}
              res.json(Posts);
            })
          });


                  router.route('/getpostbyid').get(function(req, res) {
                    var postid = req.query.postid;
                    var objectId = new ObjectID(postid);
                    Posts.find({
                      "_id": objectId}
                      , function(err, Posts) {
                        if (err) {
                          var nodemailer = require('nodemailer');
                          var transporter = nodemailer.createTransport();
                          transporter.sendMail({
                            from: 'info@myish.com',
                            to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
                            subject: 'Myish - Error - Get Post By Post Id',
                            text: 'Your error is: ' +err
                          });

                          return res.send(err);}
                        res.json(Posts);
                      })
                    });

router.route('/yayautonotify/:minval/:maxval/:notificationyaysent').get(function(req, res){
  var async = require('async');
  var minval = req.params.minval;
  var maxval = req.params.maxval;
  var notificationyaysent = req.params.notificationyaysent;
  if(notificationyaysent < 2){
    Posts.find({postyaycount : {$gte : minval, $lt : maxval}, notificationyaysent: {$lt : notificationyaysent}, postedby: {$ne: ""}}, function(err, posts){
      if(err){
        var nodemailer = require('nodemailer');
        var transporter = nodemailer.createTransport();
        transporter.sendMail({
          from: 'info@myish.com',
          to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
          subject: 'Myish - Error - Get Post By Post Id',
          text: 'Your error is: ' +err
        });
        return res.send(err);
      }
      Posts.update({postyaycount : {$gte : minval, $lt : maxval}, notificationyaysent: {$lt : notificationyaysent}, postedby: {$ne: ""}}, {
        $inc: {
          notificationyaysent: 1
        }
      },{
        multi: true
      },function(err, posts){
      });
      if(posts.length > 0){
        var androiddeviceids = [];
        var iosdeviceids = [];
        var count = 0;
        var hashdevicePost = new Object();
        var hashdevicePostedby= new Object();
        async.each(posts, function(post, callback){
          count++;
          Users.findOne({_id: ObjectID(post.postedby)}, {devicetype: 1, deviceid: 1}, function(err, user){
            if(err){
              var nodemailer = require('nodemailer');
              var transporter = nodemailer.createTransport();
              transporter.sendMail({
                from: 'info@myish.com',
                to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
                subject: 'Myish - Error - Get Post By Post Id',
                text: 'Your error is: ' +err
              });
              return res.send(err);
            }
            if(user != null){
              if(user.devicetype.toLowerCase() === 'android'){
                androiddeviceids.push(user.deviceid);
                hashdevicePost[user.deviceid]=post._id;
                hashdevicePostedby[user.deviceid]=post.postedbyusername;
              }else{
                iosdeviceids.push(user.deviceid);
                  hashdevicePost[user.deviceid]=post._id;
                  hashdevicePostedby[user.deviceid]=post.postedbyusername;
              }
            }
            if(count == posts.length){
              callback();
            }
          });
        }, function(err){
          if(err){
            return err;
          }
          if(iosdeviceids.length > 0){
            var apn  = require("apn");
            var fs = require("fs");
            var apnError = function(err){
            }
            var options = apnoptions;
            options.errorCallback = apnError;

            var feedBackOptions = {
              "batchFeedback": true,
              "interval": 300
            };

            var apnConnection, feedback;

            apnConnection = new apn.Connection(options);

            var myDevice, note;
            async.each(iosdeviceids, function(deviceid, callback){
              count++;
              if(deviceid !== ''){
                myDevice = new apn.Device(deviceid);
                note = new apn.Notification();
                note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
                note.badge = 1;
                note.sound = "ping.aiff";
                note.alert = "Yay! Your ish received its first Yay. Check it out.";
                note.payload = {'messageFrom': "myish",'postid':hashdevicePost[deviceid]};

                if(apnConnection) {
                    apnConnection.pushNotification(note, myDevice);
                }
              }
              if(count == posts.length){
                callback();
              }
            }, function(err){
              if (err) {
                return err;
              }
            });
          }
///////////////////////////////////////////////////////////////////

if(androiddeviceids.length > 0){

  var gcm = require('node-gcm');
  var sender = new gcm.Sender('AIzaSyDS2qxVIc9NBnII9vgHXoCuzXk4AV7Fz0Q');
  var myDevice, note;
  async.each(androiddeviceids, function(deviceid, callback){
    count++;
    if(deviceid !== ''){

      var FCM = require('fcm-node');
      var serverKey = 'AIzaSyCWe2-_2s4m507r9ppusKW3RfauRzRK6YU';
      var fcm = new FCM(serverKey);

      var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
          to: deviceid,
          collapse_key: 'your_collapse_key',

          data: {  //you can send only notification or only data(or include both)
              message: 'Yay! Your ish received its first Yay. Check it out.',
              postid:hashdevicePost[deviceid],
              postedby:hashdevicePostedby[deviceid]
          }
      };

      fcm.send(message, function(err, response){
          if (err) {
          //    console.log("Something has gone wrong!");
          } else {
          //    console.log("Successfully sent with response: ", response);
          }
      });


    }
    if(count == posts.length){
      callback();
    }
  }, function(err){
    if (err) {
      return err;
    }
  });
}


///////////////////////////////////////////////////

          if(androiddeviceids.length > 0){
            var gcm = require('node-gcm');
            var message = new gcm.Message({});

            if(notificationyaysent == 1){
              message.addData({
                message: "Yay! Your ish received its first Yay. Check it out."
              });
            }

            var sender = new gcm.Sender('AIzaSyDS2qxVIc9NBnII9vgHXoCuzXk4AV7Fz0Q');

            sender.send(message, { registrationTokens: androiddeviceids }, function (err, response) {
            	if(err){
                var nodemailer = require('nodemailer');
          			var transporter = nodemailer.createTransport();
          			transporter.sendMail({
          				from: 'info@myish.com',
          				to: '  raj@dignitasdigital.com',
          				subject: 'Myish - Error - Notification',
          				text: 'Your error is: ' +err
          			});
          			return res.send(err);
              }else{
                res.json({"status" : "200", "message" : "Message sent successfully.","response":response});
              }
            });
          }
        });
      }else{
        res.json({"status" : "200", "message" : "No Data Found"});
      }
    });
  }else{
    res.json({"status" : "200", "message" : "No Data Found"});
  }
});

router.route('/yaywinsautonotify/:minval/:maxval/:notificationyaysent').get(function(req, res){
  var minval = req.params.minval;
  var maxval = req.params.maxval;
  var notificationyaysent = req.params.notificationyaysent;
  var sendmessage = '';
  if(notificationyaysent == 2){
    sendmessage = "10 swipes your ish and its YAY all the way.";
  }else if(notificationyaysent == 3){
    sendmessage = "All right! Your ish is in a roll. 25 swipes. Yay wins.";
  }else{
    sendmessage = "WooHoo! 100 swipes. Yays dominate. That’s some ish!";
  }

  if(notificationyaysent > 1){
    Posts.find({postyaycount : {$gte : minval, $lt : maxval}, notificationyaysent: {$lt : notificationyaysent}, postedby: {$ne: ""}})
      .$where('this.postnaycount < this.postyaycount')
      .exec(function(err, posts){
      if(err){
        var nodemailer = require('nodemailer');
        var transporter = nodemailer.createTransport();
        transporter.sendMail({
          from: 'info@myish.com',
          to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
          subject: 'Myish - Error - Get Post By Post Id',
          text: 'Your error is: ' +err
        });
        return res.send(err);
      }
      Posts.update({postyaycount : {$gte : minval, $lt : maxval}, notificationyaysent: {$lt : notificationyaysent}, postedby: {$ne: ""}}, {
        $inc: {
          notificationyaysent: 1
        }
      },{
        multi: true
      },function(err, posts){
      });
      if(posts.length > 0){
        var async = require('async');
        var androiddeviceids = [];
        var iosdeviceids = [];
        var count = 0;
        var hashdevicePost = new Object();
        var hashdevicePostedby= new Object();
        async.each(posts, function(post, callback){
          count++;
          Users.findOne({_id: ObjectID(post.postedby)}, {devicetype: 1, deviceid: 1}, function(err, user){
            if(err){
              var nodemailer = require('nodemailer');
              var transporter = nodemailer.createTransport();
              transporter.sendMail({
                from: 'info@myish.com',
                to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
                subject: 'Myish - Error - Get Post By Post Id',
                text: 'Your error is: ' +err
              });
              return res.send(err);
            }
            if(user != null){
              if(user.devicetype.toLowerCase() === 'android'){
                androiddeviceids.push(user.deviceid);
                hashdevicePost[user.deviceid]=post._id;
                hashdevicePostedby[user.deviceid]=post.postedbyusername;
              }else{
                iosdeviceids.push(user.deviceid);
                hashdevicePost[user.deviceid]=post._id;
                hashdevicePostedby[user.deviceid]=post.postedbyusername;
              }
            }
            if(count == posts.length){
              callback();
            }
          });
        }, function(err){
          if(err){
            return err;
          }
          if(iosdeviceids.length > 0){
            var apn  = require("apn");
            var fs = require("fs");
            var apnError = function(err){
            }
            var options = apnoptions;
            options.errorCallback = apnError;

            var feedBackOptions = {
              "batchFeedback": true,
              "interval": 300
            };

            var apnConnection, feedback;

            apnConnection = new apn.Connection(options);
            var myDevice, note;
            async.each(iosdeviceids, function(deviceid, callback){
              count++;
              if(deviceid !== ''){
                myDevice = new apn.Device(deviceid);
                note = new apn.Notification();
                note.expiry = Math.floor(Date.now() / 1000) + 3600;
                note.badge = 1;
                note.sound = "ping.aiff";
                note.alert = sendmessage;
                note.payload = {'messageFrom': "myish",'postid':hashdevicePost[deviceid]};
                if(apnConnection) {
                    apnConnection.pushNotification(note, myDevice);
                }
              }
              if(count == posts.length){
                callback();
              }
            }, function(err){
              if (err) {
                return err;
              }
            });
          }
          if(androiddeviceids.length > 0){

            async.each(androiddeviceids, function(deviceid, callback){
              count++;
              if(deviceid !== ''){

                var FCM = require('fcm-node');
                var serverKey = 'AIzaSyCWe2-_2s4m507r9ppusKW3RfauRzRK6YU';
                var fcm = new FCM(serverKey);

                var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
                    to: deviceid,
                    collapse_key: 'your_collapse_key',

                    data: {  //you can send only notification or only data(or include both)
                        message: sendmessage,
                        postid:hashdevicePost[deviceid],
                        postedby:hashdevicePostedby[deviceid]
                    }
                };

                fcm.send(message, function(err, response){
                    if (err) {
                    //    console.log("Something has gone wrong!");
                    } else {
                    //    console.log("Successfully sent with response: ", response);
                    }
                });


              }
              if(count == posts.length){
                callback();
              }
            }, function(err){
              if (err) {
                return err;
              }
            });
          }

            var gcm = require('node-gcm');
            var message = new gcm.Message({});

            message.addData({
              message: sendmessage
            });

            var sender = new gcm.Sender('AIzaSyDS2qxVIc9NBnII9vgHXoCuzXk4AV7Fz0Q');

      //      sender.send(message, { registrationTokens: androiddeviceids }, function (err, response) {
      //        if(err){
      //          var nodemailer = require('nodemailer');
      //          var transporter = nodemailer.createTransport();
      //          transporter.sendMail({
      //            from: 'info@myish.com',
      //            to: '  raj@dignitasdigital.com',
      //            subject: 'Myish - Error - Notification',
      //            text: 'Your error is: ' +err
      //          });
      //          return res.send(err);
      //        }else{
      //          res.json({"status" : "200", "message" : "Message sent successfully.","response":response});
      //        }
      //      });
        //  }
        });
      }else{
        res.json({"status" : "200", "message" : "No Data Found"});
      }
    });
  }else{
    res.json({"status" : "200", "message" : "Notification sent must be atleast 2"});
  }
});



router.route('/testandroidnotifications').get(function(req,res)
{
  var FCM = require('fcm-node');
  var serverKey = 'AIzaSyCWe2-_2s4m507r9ppusKW3RfauRzRK6YU';
  var fcm = new FCM(serverKey);

  var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
      to: 'fVLzlkdiOoE:APA91bEE4KSkig1pK_WH6SUKbc4MXETt7dqZURGMxI3rjyABP4Io_q0-ROzsTqvOuign_zefAZP9XqgCLE8tHSSslo-sbjZDy98IbyfQH3NtNKNA0hmzzLJdB-5Jw_ZqzDx6T95lwgAn',
      collapse_key: 'your_collapse_key',

      data: {  //you can send only notification or only data(or include both)
          message: 'Testing',
          postid: '57d01c94b4ebf51d6ff7efd2'
      }
  };

  fcm.send(message, function(err, response){
      if (err) {
          console.log("Something has gone wrong!");
      } else {
          console.log("Successfully sent with response: ", response);
      }
  });
});


router.route('/testnotifications').get(function(req, res){
  var sendmessage = 'testingnotifications';
  var apn  = require("apn");
  var fs = require("fs");
  var apnError = function(err){
  }
  var options = apnoptions;
  options.errorCallback = apnError;

  var feedBackOptions = {
    "batchFeedback": true,
    "interval": 300
  };
var deviceid="2541D69F1A720E5690F5AAF95C2D4C5B2AA2AB9379A99362591F48043029FDA4";
  var apnConnection, feedback;

  apnConnection = new apn.Connection(options);
  var myDevice, note;

    if(deviceid !== ''){
      myDevice = new apn.Device(deviceid);
      note = new apn.Notification();
      note.expiry = Math.floor(Date.now() / 1000) + 3600;
      note.badge = 1;
      note.sound = "ping.aiff";
      note.alert = sendmessage;
      note.payload = {'messageFrom': "myish",'postid':"57d01c94b4ebf51d6ff7efd2"};
      if(apnConnection) {
          apnConnection.pushNotification(note, myDevice);
res.json({'Notification Sent':"Testing"});
      }
}
});

router.route('/naywinsautonotify/:minval/:maxval/:notificationyaysent').get(function(req, res){
  var minval = req.params.minval;
  var maxval = req.params.maxval;
  var notificationyaysent = req.params.notificationyaysent;
  var sendmessage = '';
  if(notificationyaysent == 2){
    sendmessage = "Umm. 10 swipes your ish and more NAYs than YAYs";
  }else if(notificationyaysent == 3){
    sendmessage = "Uh Oh! 25 swipes. Nay wins.";
  }else{
    sendmessage = "Whoopsie! 100 swipes. More Nays. The community has spoken.";
  }

  if(notificationyaysent > 1){
    Posts.find({postyaycount : {$gte : minval, $lt : maxval}, notificationyaysent: {$lt : notificationyaysent}, postedby: {$ne: ""}})
      .$where('this.postnaycount > this.postyaycount')
      .exec(function(err, posts){
      if(err){
        var nodemailer = require('nodemailer');
        var transporter = nodemailer.createTransport();
        transporter.sendMail({
          from: 'info@myish.com',
          to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
          subject: 'Myish - Error - Get Post By Post Id',
          text: 'Your error is: ' +err
        });
        return res.send(err);
      }
      Posts.update({postyaycount : {$gte : minval, $lt : maxval}, notificationyaysent: {$lt : notificationyaysent}, postedby: {$ne: ""}}, {
        $inc: {
          notificationyaysent: 1
        }
      },{
        multi: true
      },function(err, posts){
      });
      if(posts.length > 0){
        var async = require('async');
        var androiddeviceids = [];
        var iosdeviceids = [];
        var count = 0;
        var hashdevicePost = new Object();
        var hashdevicePostedby= new Object();
        async.each(posts, function(post, callback){
          count++;
          Users.findOne({_id: ObjectID(post.postedby)}, {devicetype: 1, deviceid: 1}, function(err, user){
            if(err){
              var nodemailer = require('nodemailer');
              var transporter = nodemailer.createTransport();
              transporter.sendMail({
                from: 'info@myish.com',
                to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
                subject: 'Myish - Error - Get Post By Post Id',
                text: 'Your error is: ' +err
              });
              return res.send(err);
            }
            if(user != null){
              if(user.devicetype.toLowerCase() === 'android'){
                androiddeviceids.push(user.deviceid);
                hashdevicePost[user.deviceid]=post._id;
                hashdevicePostedby[user.deviceid]=post.postedbyusername;
              }else{
                iosdeviceids.push(user.deviceid);
                hashdevicePost[user.deviceid]=post._id;
                hashdevicePostedby[user.deviceid]=post.postedbyusername;
              }
            }
            if(count == posts.length){
              callback();
            }
          });
        }, function(err){
          if(err){
            return err;
          }
          if(iosdeviceids.length > 0){
            var apn  = require("apn");
            var fs = require("fs");
            var apnError = function(err){
              console.log("APN Error:", err);
              console.log("not working...");
            }
            var options = apnoptions;
            options.errorCallback = apnError;

            var feedBackOptions = {
              "batchFeedback": true,
              "interval": 300
            };

            var apnConnection, feedback;

            apnConnection = new apn.Connection(options);
            var myDevice, note;
            async.each(iosdeviceids, function(deviceid, callback){
              count++;
              if(deviceid !== ''){
                myDevice = new apn.Device(deviceid);
                note = new apn.Notification();
                note.expiry = Math.floor(Date.now() / 1000) + 3600;
                note.badge = 1;
                note.sound = "ping.aiff";
                note.alert = sendmessage;
                note.payload = {'messageFrom': "myish",'postid':hashdevicePost[deviceid]};
                if(apnConnection) {
                    apnConnection.pushNotification(note, myDevice);
                }
              }
              if(count == posts.length){
                callback();
              }
            }, function(err){
              if (err) {
                return err;
              }
            });
          }
          if(androiddeviceids.length > 0)
          {
            async.each(androiddeviceids, function(deviceid, callback){
              count++;
              if(deviceid !== ''){

                var FCM = require('fcm-node');
                var serverKey = 'AIzaSyCWe2-_2s4m507r9ppusKW3RfauRzRK6YU';
                var fcm = new FCM(serverKey);

                var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
                    to: deviceid,
                    collapse_key: 'your_collapse_key',

                    data: {  //you can send only notification or only data(or include both)
                        message: sendmessage,
                        postid:hashdevicePost[deviceid],
                        postedby:hashdevicePostedby[deviceid]
                    }
                };

                fcm.send(message, function(err, response){
                    if (err) {
                    //    console.log("Something has gone wrong!");
                    } else {
                    //    console.log("Successfully sent with response: ", response);
                    }
                });


              }
              if(count == posts.length){
                callback();
              }
            }, function(err){
              if (err) {
                return err;
              }
            });






            var gcm = require('node-gcm');
            var message = new gcm.Message({});

            message.addData({
              message: sendmessage
            });

            var sender = new gcm.Sender('AIzaSyBF3y0-EzHl10Jg6GSsJtLPxHMEUDx-9hQ');

        /*    sender.send(message, {registrationTokens: androiddeviceids }, function (err, response) {
              if(err){
                var nodemailer = require('nodemailer');
                var transporter = nodemailer.createTransport();
                transporter.sendMail({
                  from: 'info@myish.com',
                  to: '  raj@dignitasdigital.com',
                  subject: 'Myish - Error - Notification',
                  text: 'Your error is: ' +err
                });
                return res.send(err);
              }else{
                res.json({"status" : "200", "message" : "Message sent successfully.","response":response});
              }
            });*/
          }
        });
      }else{
        res.json({"status" : "200", "message" : "No Data Found"});
      }
    });
  }else{
    res.json({"status" : "200", "message" : "Notification sent must be atleast 2"});
  }
});
  module.exports = router;
