var questions = require('../models/questions');
var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var randomstring = require("randomstring");


router.route('/questions')
.get(function(req, res) {
  questions.find(function(err, users) {
    if (err) {
      return res.send(err);
    }
    res.json(questions);
  });
})