var Redeemptions = require('../models/redeemption');
var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var randomstring = require("randomstring");
var path = require("path");
var myconstants = require("../coustom-constants");

// Define Routes
router.route('/redeem/users/:page*?').get(pagination, function(req, res) {
  getPaginationUrl(req, function(cleanUrl){
    Redeemptions.find({})
    .skip((res.locals.page - 1) * myconstants.PAGINATION_LIMIT)
    .limit(myconstants.PAGINATION_LIMIT)
    .sort('-timestamp')
    .select('-password')
    .exec(function(err, users) {
      if(res.locals.prev)
      var prevurl = req.protocol + '://' + req.get('host') + cleanUrl + "/" + (res.locals.page-1);
      if(res.locals.next)
      var nexturl = req.protocol + '://' + req.get('host') + cleanUrl + "/" + (res.locals.page+1);
      users.push({previous: prevurl, next: nexturl});
      res.json(users);
    });
  });
  // console.log(req.originalUrl, req.path);
}).post(function(req, res) {
  var redeemptions = new Redeemptions(req.body);
  redeemptions.save(function(err,userObjectId) {
    if (err) {
      sendMail(myconstants.FROM_EMAIL, myconstants.TO_MAIL, 'This is a test message for user '+err, 'Welcome to Myish', res);
    }
    res.send(userObjectId.id);
  });
});

// Our Custom Functions
// pagination middleware function sets some
// local view variables that any view can use
function pagination(req, res, next) {
    var page = parseInt(req.params.page) || 1,
        num = page * myconstants.PAGINATION_LIMIT;
    Redeemptions.count(function(err, total) {
        res.locals.total = total;
        res.locals.pages = Math.ceil(total / myconstants.PAGINATION_LIMIT);
        res.locals.page = page;
        if (num <= total && page != 1) res.locals.prev = true;
        if (num >= myconstants.PAGINATION_LIMIT && num < total) res.locals.next = true;
        next();
    });
}

// Removing Page Number From Current Request
function getPaginationUrl(req, callback){
  var url = req.originalUrl;
  if (!req.params.page) {
    return callback(url);
  }else{
    callback(url.replace(url.substring(url.lastIndexOf('/')), ''));
  }
}

// Sending Mails to admin or users when required
function sendMail(from, to, message, sub, response){
  var nodemailer = require('nodemailer');
  var transporter = nodemailer.createTransport();
  transporter.sendMail({
    from: from,
    to: to,
    subject: sub,
    text: message
  });
  return response.send(message);
}

module.exports = router;
