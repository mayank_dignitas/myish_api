var Users = require('../models/users');
var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var randomstring = require("randomstring");
var ObjectID = require('mongodb').ObjectID;


router.route('/users')
.get(function(req, res) {
  Users.find(function(err, users) {
    if (err) {
      return res.send(err);
    }
    res.json(users);
  });
})
.post(function(req,res) {
  var users = new Users(req.body);
  users.save(function(err,userObjectId) {
    if (err) {
      return res.send(err);
    }

    var nodemailer = require('nodemailer');
    var transporter = nodemailer.createTransport();
    transporter.sendMail({
      from: 'info@myish.com',
      to: 'rishi@dignitasdigital.com ; mayank@dignitasdigital ; amit@dignitasdigital.com',
      subject: 'Welcome to MyQ',
      text: 'New User Joined'
    });
    res.send(userObjectId.id);
  });
});

router.route('/validate').get(function(req, res) {
  var emailaddressvar = req.query.emailaddress;
  var passwordvar = req.query.password;
  var passwordhashvar = crypto.createHash('md5').update(passwordvar).digest("hex");
    Users.find( {emailaddress: emailaddressvar, password:passwordhashvar },function(err, users) {
      if (err) {
        return res.send(err);
      }

      /*var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
        subject: 'Myish - Login ',
        text: 'Your user is: ' +emailaddressvar
      });*/




    res.json(users);
  });
});

router.route('/validateid').get(function(req, res) {
  var emailaddressvar = req.query.emailaddress;
  var passwordvar = req.query.password;
  var passwordhashvar = crypto.createHash('md5').update(passwordvar).digest("hex");
  Users.find( {emailaddress: emailaddressvar, password:passwordhashvar },"_id",function(err, users) {
    if (err) {
      return res.send(err);
    }
  //  var userid = users._id;

  var nodemailer = require('nodemailer');
  var transporter = nodemailer.createTransport();
  transporter.sendMail({
    from: 'info@myish.com',
    to: 'rishi@dignitasdigital.com ; mayank@dignitasdigital.com; amit@dignitasdigital.com',
    subject: 'Myish - Login',
    text: 'Your user is: ' +emailaddressvar
  });




    res.send(users);
  });
});


router.route('/users/:id').get(function(req, res) {
  Users.findOne({ _id: req.params.id}, function(err, users) {
    if (err) {
      return res.send(err);
    }

    res.json(users);
  });
});



//adding friends



router.route('/addfriends').post(function(req, res) {

  var userid = req.body.userid;

  var objectId = new ObjectID(userid);
  var friendid = req.body.friendid;
  var friendObjectId= new ObjectID(friendid);
  var friendprofilepicture = req.body.friendprofilepictureURL;
  Users.update({
    "_id": objectId
  },
  {
    "$push": {
      "friends": {
        "userId":friendObjectId,"timestamp":new Date(),"profilepictureURL":friendprofilepicture
      }
    }
    
  }
  , function(err, result) {
    if (err)  {
      //return res.send(err);
      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; mayank@dignitasdigital.com',
        subject: 'Myish - Error - AddFollower',
        text: 'Your error is: ' +err
      });
      return res.send(err);
    }

    res.send({ message: 'Friend Added' });
  });
});


//adding groups

router.route('/addgroups').post(function(req, res) {

  var userid = req.body.userid;

  var objectId = new ObjectID(userid);
  var groupId = req.body.groupid;
  var groupObjectId= new ObjectID(groupId);
  var groupName = req.body.groupname;
  Users.update({
    "_id": objectId
  },
  {
    "$push": {
      "groups": {
        "groupId":groupObjectId,"timestamp":new Date(),"groupName":groupName
      }
    }
    
  }
  , function(err, result) {
    if (err)  {
      //return res.send(err);
      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; mayank@dignitasdigital.com',
        subject: 'Myish - Error - AddFollower',
        text: 'Your error is: ' +err
      });
      return res.send(err);
    }

    res.send({ message: 'Group Added' });
  });
});


//adding questions


router.route('/addquestionstouser').post(function(req, res) {

  var userid = req.body.userid;

  var objectId = new ObjectID(userid);
  var questionId = req.body.questionid;
  var questionObjectId= new ObjectID(questionId);
  var questionText = req.body.questiontext;
  var questionImage = req.body.questionimage;
  var questionCorrectAnswer = req.body.questioncorrectanswer;
  var questionPoints = req.body.questionpoints;
  var questionCorrectness = req.body.questioncorrectness;

  /*if(questionCorrectness="Correct"){
  
   Users.update({
    "_id": objectId
  },
  {
    "$inc": {
      "CorrectAnswers":+1
    }
  }
  , function(err, result) {
  

    res.send({ message: 'Points Added' });
  });


  }*/

  


  Users.update({
    "_id": objectId
  },
  {
    "$push": {
      "questions": {
        "questionId":questionObjectId,"timestamp":new Date(),"questionText":questionText,"questionImage":questionImage,"questionCorrectAnswer":questionCorrectAnswer,"questionPoints":questionPoints
      }
    }


    
  },

  {
    "$inc": {
      "points":+questionPoints
    }
  }
  , function(err, result) {
    if (err)  {
      //return res.send(err);
      return res.send(err);
    }

    res.send({ message: 'Question Added with Answer and Points' });
  });





});


//update points


router.route('/updatepoints').post(function(req, res) {
  var userid = req.body.userid;
  var objectId = new ObjectID(userid);
  var points = req.body.points

  Users.update({
    "_id": objectId
  },
  {
    "$inc": {
      "points":+2
    }
  }
  , function(err, result) {
    if (err)  {

      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'mayank@dignitasdigital.com',
        subject: 'Myish - Error - Add Points',
        text: 'Your error is: ' +err
      });


      return res.send(err);
    }

    res.send({ message: 'Points Added' });
  });
});









/*router.route('/addfollower').post(function(req, res) {

  var userid = req.body.userid;
  var objectId = new ObjectID(userid);
  var followerid = req.body.followerid;
  var followerObjectId= new ObjectID(followerid);
  var followerusername = req.body.followerusername;
  var followerprofilepicture = req.body.followerprofilepictureURL;
  Users.update({
    "_id": objectId
  },
  {
    "$push": {
      "followers": {
        "followerid":followerObjectId,"timestamp":new Date(),"followerusername":followerusername,"followerprofilepictureURL":followerprofilepicture
      }
    },
    "$inc":{
      "followerscount":+1
    }
  }
  , function(err, result) {
    if (err)  {
      //return res.send(err);
      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
        subject: 'Myish - Error - AddFollower',
        text: 'Your error is: ' +err
      });
      return res.send(err);
    }

    res.send({ message: 'Follower Added' });
  });
});





router.route('/addpoints').post(function(req, res) {
  var userid = req.body.userid;
  var objectId = new ObjectID(userid);

  Users.update({
    "_id": objectId
  },
  {
    "$inc": {
      "points":+5
    }
  }
  , function(err, result) {
    if (err)  {

      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
        subject: 'Myish - Error - Add Points',
        text: 'Your error is: ' +err
      });


      return res.send(err);
    }

    res.send({ message: 'Points Added' });
  });
});
*/
router.route('/changepassword').post(function(req, res) {
  var userid = req.body.userid;
  var objectId = new ObjectID(userid);
  var newpassword = req.body.password;
  var passwordhashvar = crypto.createHash('md5').update(newpassword).digest("hex");

  Users.update({
    "_id": objectId
  },
  {
    "$set": {
      "password":passwordhashvar }}
      , function(err, result) {
        if (err)  {

          var nodemailer = require('nodemailer');
          var transporter = nodemailer.createTransport();
          transporter.sendMail({
            from: 'info@myish.com',
            to: 'rishi@dignitasdigital.com ; mayank@dignitasdigital.com ; amit@dignitasdigital.com',
            subject: 'Myish - Error - Change Password',
            text: 'Your error is: ' +err
          });
      return res.send(err);
    }

        res.send({ message: 'Password Updated' });
      });
    });


    router.route('/forgotpassword').post(function(req, res) {
      var emailaddress = req.body.emailaddress;
      var newpassword = randomstring.generate(7);

      var passwordhashvar = crypto.createHash('md5').update(newpassword).digest("hex");
      Users.update({
        "emailaddress": emailaddress
      },
      {
        "password":passwordhashvar }
        , function(err, result) {
          if (err)  {

            /*var nodemailer = require('nodemailer');
            var transporter = nodemailer.createTransport();
            transporter.sendMail({
              from: 'info@myish.com',
              to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
              subject: 'Myish - Error - Forgot Password',
              text: 'Your error is: ' +err
            });*/

      return res.send(err);
    }

          res.send({ message: 'Password Updated' });
        });


        var nodemailer = require('nodemailer');
        var transporter = nodemailer.createTransport();
        transporter.sendMail({
          from: 'info@myish.com',
          to: emailaddress,
          subject: 'Myish - New Password',
          text: 'Hi,\r\n\r\tYour new password is: ' +newpassword+ '\r\n\r\n\r\nThanks\r\nMyish Team'
        });
      });

      /*router.route('/followers').get(function(req, res) {
        var userid = req.query.userid;
        var objectId = new ObjectID(userid);
        Users.find({
          "followers.followerid": {"$ne":objectId}}
          , function(err, Posts) {
            if (err)  {
      return res.send(err);
    }

            res.json(Posts);
          });
        });
*/
// Raj Changes
  // router.route('/finduser').get(function(req, res) {
  //         var userid = req.query.userid;
  //         var objectId = new ObjectID(userid);
  //         Users.find( {"_id": objectId},function(err, users) {
  //           if (err) {
  //
  //             var nodemailer = require('nodemailer');
  //             var transporter = nodemailer.createTransport();
  //             transporter.sendMail({
  //               from: 'info@myish.com',
  //               to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
  //               subject: 'Myish - Error - Find User',
  //               text: 'Your error is: ' +err
  //             });
  //
  //             return res.send(err);
  //           }
  //           res.json(users);
  //         });
  //       });


/*router.route('/finduser').get(function(req, res) {
var userid = req.query.userid;
var objectId = new ObjectID(userid);
Users.aggregate([{
$unwind:  "$postscreated"
}, {
$match: {
  "_id": userid
}
}, {
$sort: {
  'postscreated.timestamp': -1
}
}, {
$group: {
  _id:{
    _id:"$_id",
    emailaddress: "$emailaddress",
    username: "$username",
    password: "$password",
    lastlogintimestamp: "$lastlogintimestamp",
    longitude: "$longitude",
    lattitude: "$lattitude",
    postscreatedcount: "$postscreatedcount",
    followingcount: "$followingcount",
    followerscount: "$followerscount",
    redemption: "$redemption",
    following: "$following",
    followers: "$followers",
    postsseen: "$postsseen",
    aboutme: "$aboutme",
    points: "$points",
    googleplusid: "$googleplusid",
    fbid: "$fbid",
    profilepicture: "$profilepicture",
    gender: "$gender",
    source: "$source",
    deviceid: "$deviceid",
    devicetype: "$devicetype",
    'timestamp': '$timestamp'
  },
  postscreated: {
    $push: "$postscreated"
  }
}
}],function(err, users) {
if (err) {
  var nodemailer = require('nodemailer');
  var transporter = nodemailer.createTransport();
  transporter.sendMail({
    from: 'info@myish.com',
    to: 'raj@dignitasdigital.com',
    subject: 'Myish - Error - Find User',
    text: 'Your error is: ' +err
  });
  return res.send(err);
}
if(users.length > 0) {
  var myjson = users[0]._id;
  myjson.postscreated = users[0].postscreated;
  return res.json([myjson]);
}
Users.findOne({"_id": objectId}, function(err, user) {
 if (err) {
   return res.send(err);
 }
 res.json([user]);
});
});
});
*/
// Raj Changes ends

/*router.route('/newfinduser/:userid').get(function(req, res) {
var userid = req.params.userid;
var objectId = new ObjectID(userid);
Users.find( {"_id": objectId},{postscreated:{$slice: -2}},function(err, users) {
if (err) {

  var nodemailer = require('nodemailer');
  var transporter = nodemailer.createTransport();
  transporter.sendMail({
    from: 'info@myish.com',
    to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
    subject: 'Myish - Error - Find User',
    text: 'Your error is: ' +err
  });

  return res.send(err);
}
res.json(users);
});
});
*/
/*router.route('/newfindusergetmoreposts/:userid/:page').get(function(req, res) {
var userid = req.params.userid;
var objectId = new ObjectID(userid);
var page = req.params.page;
var offset = (page != 'undefind' && page != 0) ? page*2 : 1;
Users.aggregate(
[{$match:{_id:objectId}},
{
 $project: {
   count: {$size: "$postscreated"},
   _id: 0
 }
},
{$limit:1}
],function(err, user){
if(user[0].count >= offset){
  Users.find( {"_id": objectId},{postscreated: {$slice: [-offset, 2]}, _id: 0, foo: 1 },function(err, users) {
    if (err) {
      var nodemailer = require('nodemailer');
      var transporter = nodemailer.createTransport();
      transporter.sendMail({
        from: 'info@myish.com',
        to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
        subject: 'Myish - Error - Find User',
        text: 'Your error is: ' +err
      });
      return res.send(err);
    }
    res.json(users);
  });
}else{
  res.json({"status":"200","message":"No more posts found"});
}
});
});



*/  
/*router.route('/recordtimestamp').post(function(req, res) {
          var userid = req.body.userid;
          var objectId = new ObjectID(userid);
          Users.update({
            "_id": objectId
          },
          {
            "$set": {
              "lastlogintimestamp":new Date() }}
              , function(err, result) {
                if (err) {

                  var nodemailer = require('nodemailer');
                  var transporter = nodemailer.createTransport();
                  transporter.sendMail({
                    from: 'info@myish.com',
                    to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
                    subject: 'Myish - Error - Record Timestamp',
                    text: 'Your error is: ' +err
                  });



                  return res.send(err);
                };
                res.send({ message: 'Login Timestamp Updated' });
              });
            });
*/
   


                      router.route('/updateusername').post(function(req, res) {
                        var userid = req.body.userid;
                        var objectId = new ObjectID(userid);
                        var username = req.body.username;

                        Users.update({
                          "_id": objectId
                        },
                        {
                          "$set": {
                            "username":username }}
                            , function(err, result) {
                              if (err)  {

                                var nodemailer = require('nodemailer');
                                var transporter = nodemailer.createTransport();
                                transporter.sendMail({
                                  from: 'info@myish.com',
                                  to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
                                  subject: 'Myish - Error - Update User Name',
                                  text: 'Your error is: ' +err
                                });


                            return res.send(err);
                          }

                              res.send({ message: 'username Updated' });
                            });
                          });


                          router.route('/updateaboutme').post(function(req, res) {
                            var userid = req.body.userid;
                            var objectId = new ObjectID(userid);
                            var aboutme = req.body.aboutme;

                            Users.update({
                              "_id": objectId
                            },
                            {
                              "$set": {
                                "aboutme":aboutme }}
                                , function(err, result) {
                                  if (err)  {

                                    var nodemailer = require('nodemailer');
                                    var transporter = nodemailer.createTransport();
                                    transporter.sendMail({
                                      from: 'info@myish.com',
                                      to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
                                      subject: 'Myish - Error - Update About Me',
                                      text: 'Your error is: ' +err
                                    });
                                return res.send(err);
                              }

                                  res.send({ message: 'about me Updated' });
                                });
                              });


                              router.route('/updatedob').post(function(req, res) {
                                var userid = req.body.userid;
                                var objectId = new ObjectID(userid);
                                var dob = req.body.dob;

                                Users.update({
                                  "_id": objectId
                                },
                                {
                                  "$set": {
                                    "dob":dob }}
                                    , function(err, result) {
                                      if (err)  {


                                        var nodemailer = require('nodemailer');
                                        var transporter = nodemailer.createTransport();
                                        transporter.sendMail({
                                          from: 'info@myish.com',
                                          to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
                                          subject: 'Myish - Error - Update DOB',
                                          text: 'Your error is: ' +err
                                        });
                                    return res.send(err);
                                  }

                                      res.send({ message: 'DOB Updated' });
                                    });
                                  });

                                  router.route('/updategender').post(function(req, res) {
                                    var userid = req.body.userid;
                                    var objectId = new ObjectID(userid);
                                    var gender = req.body.gender;

                                    Users.update({
                                      "_id": objectId
                                    },
                                    {
                                      "$set": {
                                        "gender":gender }}
                                        , function(err, result) {
                                          if (err)  {
                                            var nodemailer = require('nodemailer');
                                            var transporter = nodemailer.createTransport();
                                            transporter.sendMail({
                                              from: 'info@myish.com',
                                              to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
                                              subject: 'Myish - Error - Update Gender',
                                              text: 'Your error is: ' +err
                                            });

                                        return res.send(err);
                                      }

                                          res.send({ message: 'DOB Updated' });
                                        });
                                      });



                                      router.route('/updateprofile').post(function(req, res) {
                                        var userid = req.body.userid;
                                        var objectId = new ObjectID(userid);
                                        var aboutme = req.body.aboutme;
                                        var emailaddress= req.body.emailaddress;
                                        var username= req.body.username;
                                        var gender= req.body.gender;

                                        Users.update({
                                          "_id": objectId
                                        },
                                        {
                                          "$set": {
                                            "aboutme":aboutme, "emailaddress":emailaddress, "username":username, "gender":gender }}
                                            , function(err, result) {
                                              if (err)  {
                                                var nodemailer = require('nodemailer');
                                                var transporter = nodemailer.createTransport();
                                                transporter.sendMail({
                                                  from: 'info@myish.com',
                                                  to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
                                                  subject: 'Myish - Error - Update Profile',
                                                  text: 'Your error is: ' +err
                                                });


                                            return res.send(err);
                                          }
                                              res.send({ message: 'Profile Updated' });
                                            });
                                          });


                                          router.route('/sociallogin').post(function(req, res) {

                                            var emailaddress= req.body.emailaddress;
                                            var password= req.body.password;
                                            var username= req.body.username;
                                            var gender= req.body.gender;
                                            var aboutme = req.body.aboutme;
                                            var dob= req.body.dob;
                                            var devicetype=req.body.devicetype;
                                            var deviceid=req.body.deviceid;
                                            var source = req.body.source;
                                            var profilepicture= req.body.profilepicture;
                                            var fbid= req.body.fbid;
                                            var googleplusid= req.body.googleplusid;
                                            var passwordhashvar = crypto.createHash('md5').update(password).digest("hex");

                                            Users.findOne({"emailaddress":emailaddress},function(err,users)
                                            {
                    if (err)  {
                                  var nodemailer = require('nodemailer');
                                  var transporter = nodemailer.createTransport();
                                  transporter.sendMail({
                                    from: 'info@myish.com',
                                    to: 'rishi@dignitasdigital.com ; nicksriv@gmail.com ; amit@dignitasdigital.com',
                                    subject: 'Myish - Error - Update Gender',
                                    text: 'Your error is: ' +err
                                  });

                                  return res.send(err);
                                }

                                  if(users==null)
                                                {
                                                  var users = new Users(req.body);
                                                  users.save({
                                                    "emailaddress":emailaddress,
                                                    "username":username,
                                                    "gender":gender,
                                                    "aboutme":aboutme,
                                                    "dob":dob,
                                                    "devicetype":devicetype,
                                                    "deviceid":deviceid,
                                                    "source":source,
                                                    "profilepicture":profilepicture,
                                                    "fbid":fbid,
                                                    "googleplusid":googleplusid,
                                                    "password":passwordhashvar,
                                                    "timestamp":new Date(),
                                                    "points":0

                                                    });
                                                  res.json([users]);
                                                }
                                                else {
                                                    res.json([users]);
                                                }
                                          });
                                        });


              
router.route('/updatedeviceid').post(function(req, res){
var id = req.body.id;
var deviceid = req.body.deviceid;
Users.update({_id: ObjectID(id)},{
$set:{
  deviceid: deviceid
}
},function(err, user){
if(err){
  var nodemailer = require('nodemailer');
  var transporter = nodemailer.createTransport();
  transporter.sendMail({
    from: 'info@focally.com',
    to: 'raj@dignitasdigital.com',
    subject: 'Focally - Error - Add Dish',
    text: 'Your error is: ' +err
  });
  return res.send(err);
}
if(user.nModified > 0){
  res.json({status: 200, message: "Deviceid updated"});
}else{
  res.json({status: 200, message: "Deviceid not updated"});
}
});
});

module.exports = router;
