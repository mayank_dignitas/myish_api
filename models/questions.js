var mongoose=require('mongoose'),
 crypto = require('crypto'),
 uuid = require('node-uuid');
var Schema=mongoose.Schema;

var Followers = new Schema ({
followerid : {type: String},
timestamp : {type: Date},
followerusername:{type:String},
followerprofilepictureURL:{type:String}
})

var Following = new Schema ({
followingid : {type: String},
timestamp : {type: Date},
followingusername:{type:String},
followingprofilepictureURL:{type:String}
})

var Redemption = new Schema ({
couponid : {type: String},
timestamp : {type: Date}
})

var PostsCreated = new Schema(
{
    postid:{type: String},
    timestamp : {type: Date},
    postname:{type:String},
    postimage:{type:String}
})

var PostsSeen = new Schema(
  {
    postid:{type: String},
    timestamp : {type: Date}
  }
)

var usersSchema = new Schema({
  user_id: { type: String },
  username:{ type: String },
  password:{ type: String, required: true },
  emailaddress:{ type: String, required: true, unique: true },
  devicetype:{type:String,default:""},
  deviceid:{type:String,default:""},
  source:{type:String,default:""},
  gender:{type:String,default:""},
  dob:Date,
  profilepicture:{type:String,default:""},
  fbid:{type:String,default:""},
  googleplusid:{type:String,default:""},
  points:{type:Number,default:0},
  aboutme:{type:String,default:""},
  timestamp:{type:Date, default:Date.now},
  lastlogintimestamp:{type:Date},
  postscreated: [PostsCreated],
  postsseen: [PostsSeen],
  followers : [Followers],
  following : [Following],
  redemption : [Redemption],
  followerscount:{type:Number, default:0},
  followingcount:{type:Number, default:0},
  postscreatedcount:{type:Number, default:0},
  lattitude:{type:String,default:""},
  longitude:{type:String,default:""}
});

usersSchema.pre('save',
    function(next) {
        if (this.password) {
            var md5 = crypto.createHash('md5');
            this.password = crypto.createHash('md5').update(this.password).digest("hex");
        }
        next();
    }
);

module.exports = mongoose.model('Questions', usersSchema);
