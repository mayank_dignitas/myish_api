'use strict';

/**
* Module dependencies
*/
var mongoose=require('mongoose'),
    Schema=mongoose.Schema,
    crypto = require('crypto'),
    uuid = require('node-uuid'),

    passportLocalMongoose = require('passport-local-mongoose'),
    validator = require('validator'),// ,
    // generatePassword = require('generate-password'),
    owasp = require('owasp-password-strength-test');

  /**
  * A Validation function for local strategy properties
  */
  var validateLocalStrategyProperty = function (property) {
    return ((this.provider !== 'local' && !this.updated) || property.length);
  };

  /**
  * A Validation function for local strategy email
  */
  var validateLocalStrategyEmail = function (email) {
    return ((this.provider !== 'local' && !this.updated) || validator.isEmail(email, { require_tld: false }));
  };


var RedeemedUsers = new Schema ({
  user_id: {
    type: Schema.ObjectId,
    trim: true
  }
})
var Offer = new Schema ({
  url: {
    type: String,
    trim: true,
    default: ''
  },
  title: {
    type: String,
    trim: true,
    default: ''
  },
  image: {
    type: String,
    trim: true,
    default: ''
  },
  percentage: {
    type: Number,
    default: 0
  },
  latitude: {
    type: String,
    trim: true,
    default: ''
  },
  longitude: {
    type: String,
    trim: true,
    default: ''
  },
  start_date: {
    type: Date,
    default: '',
    trim: true
  },
  end_date: {
    type: Date,
    default: '',
    trim: true
  },
  redeemed_yes: [RedeemedUsers],
  redeemed_no: [RedeemedUsers],
  redeemed_skip: [RedeemedUsers],
  deleted: {
    type: Boolean,
    default: false
  },
  useryays: {
    type: Number,
    default: 0
  },
  usernays: {
    type: Number,
    default: 0
  },
  userskips: {
    type: Number,
    default: 0
  },
  location: {
    type: String,
    trim: true,
    default: ''
  }
})

var redeemptionSchema = new Schema({
  brand_id: {
    type: String,
    default: '',
    trim: true
  },
  brandname:{
    type: String,
    default: '',
    trim: true
  },
  username:{
    type: String,
    trim: true,
    index: {
      unique: 'Username already exists',
      sparse: true
    },
    lowercase: true,
    validate: [validateLocalStrategyProperty, 'Please fill in your username']
  },
  personincharge:{
    type: String,
    default: '',
    trim: true
  },
  password:{
    type: String,
    required: true
  },
  emailaddress:{
    type: String,
    index: {
      unique: 'Email already exists',
      sparse: true // For this to work on a previously indexed field, the index must be dropped & the application restarted.
    },
    lowercase: true,
    trim: true,
    required: 'Please fill in a email',
    validate: [validateLocalStrategyEmail, 'Please fill a valid email address']
  },
  contactinfo: {
    type: String,
    trim: true,
    default: ''
  },
  start_date: {
    type: Date,
    default: '',
    trim: true
  },
  end_date: {
    type: Date,
    default: '',
    trim: true
  },
  offer: [Offer],
  package: {
    type: String,
    trim: true,
    default: ''
  },
  logopath: {
    type: String,
    trim: true,
    default: ''
  },
  devicetype: {
    type:String,
    default:"",
    trim: true
  },
  deviceid: {
    type:String,
    default:"",
    trim: true
  },
  lastlogintimestamp:{
    type: Date,
    default: Date.now,
    trim: true
  },
  timestamp: {
    type: Date,
    default: Date.now,
    trim: true
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },
});

redeemptionSchema.pre('save',
    function(next) {
        if (this.password) {
            var md5 = crypto.createHash('md5');
            this.password = crypto.createHash('md5').update(this.password).digest("hex");
        }
        next();
    }
);

redeemptionSchema.plugin(passportLocalMongoose);

/**
* Create instance method for hashing a password
*/
redeemptionSchema.methods.hashPassword = function (password) {
  if (this.password) {
    // var md5 = crypto.createHash('md5');
    this.password = crypto.createHash('md5').update(this.password).digest('hex');
  }
};

redeemptionSchema.methods.validatePass = function (password) {
  if (password) {
    // var md5 = crypto.createHash('md5');
    // return crypto.createHash('md5').update(password).digest('hex');
    // console.log(password);
    return crypto.createHash('md5').update(password).digest('hex');
  }
};

/**
* Create instance method for authenticating user
*/
redeemptionSchema.methods.authenticate = function (password) {
  console.log(password);
  return this.password === this.validatePass(password);
};
// Export the model.
module.exports = mongoose.model('Redeemptions', redeemptionSchema);
//module.exports = mongoose.model('movie', MovieSchema);
