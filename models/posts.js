var mongoose=require('mongoose'),
 crypto = require('crypto'),
 uuid = require('node-uuid');
var Schema=mongoose.Schema;

var Comments = new Schema({
 comments:{type:String},
 userid:{type:String},
 timestamp:{type:Date},
 userimage:{type:String},
 username:{type:String}
 })

var PostYays= new Schema({
userid:{type:String},
timestamp:{type:Date}
})

var PostNays= new Schema({
userid:{type:String},
timestamp:{type:Date}
})

var PostSkip= new Schema({
userid:{type:String},
timestamp:{type:Date}
})

var PostSeenbyUser = new Schema({
userid:{type:String},
timestamp:{type:Date}
})

var ReportAbuse = new Schema({
   userid:{type:String},
   timestamp:{type:Date}
})

var postsSchema = new Schema ({
postsid: {type: String},
postname : {type:String,default:""},
postimage: {type:String,default:""},
postedby:  {type:String,default:""},
postedbyusername: {type:String,default:""},
postedbyprofilepicture: {type:String,default:""},
category: {type:String,default:""},
timestamp: {type:Date, default:Date.now},
comments:[Comments],
reportabuse:[ReportAbuse],
postyays:[PostYays],
postyaycount:{type:Number, default:0},
commentcount:{type:Number, default:0},
notificationyaysent:{type:Number, default:0},
postnays:[PostNays],
postnaycount:{type:Number, default:0},
postskip:[PostSkip],
postskipcount:{type:Number, default:0},
postseenbyuser:[PostSeenbyUser],
lattitude:{type:String,default:""},
longitude:{type:String,default:""}
});

module.exports = mongoose.model('Posts', postsSchema)
