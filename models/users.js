var mongoose=require('mongoose'),
 crypto = require('crypto'),
 uuid = require('node-uuid');
var Schema=mongoose.Schema;


var Friends = new Schema ({
userId:{type:String},
timestamp : {type: Date},
profilepictureURL :{type:String}
})



var Questions = new Schema(
{
    questionId:{type: String},
    timestamp : {type: Date},
    questionText:{type:String},
    questionImage:{type:String},
    questionCorrectAnswer : {type:String},
    questionPoints:{type:Number}
})

var Groups = new Schema(
{
    groupId:{type: String},
    timestamp : {type: Date},
    groupname:{type:String},
    
})

var Challenges = new Schema(
{

}
)




var usersSchema = new Schema({
  user_id: { type: String },
  emailaddress:{ type: String, required: true, unique: true },
  password:{ type: String, required: true },
  username:{ type: String },
  longitude:{type:String,default:""},
  lattitude:{type:String,default:""},
  questions:[Questions],
  groups:[Groups],
  timestamp:{type:Date, default:Date.now},
  aboutme:{type:String,default:""},
  points:{type:Number,default:0},
  googleplusid:{type:String,default:""},
  fbid:{type:String,default:""},
  profilepicture:{type:String,default:""},
  gender:{type:String,default:""},
  source:{type:String,default:""},
  deviceid:{type:String,default:""},
  devicetype:{type:String,default:""},
  dob:Date,
  college:{type:String,default:""},
  batch:{type:String, default:""},
  lastlogintimestamp:{type:Date},
  friends:[Friends],
  challenges:[Challenges],
  CorrectAnswers:{type:Number,default:0}
});

usersSchema.pre('save',
    function(next) {
        if (this.password) {
            var md5 = crypto.createHash('md5');
            this.password = crypto.createHash('md5').update(this.password).digest("hex");
        }
        next();
    }
);

module.exports = mongoose.model('Users', usersSchema);
